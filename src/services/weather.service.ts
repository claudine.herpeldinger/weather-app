import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class WeatherService {
  
  
  constructor(private http: HttpClient) {}

  getWeather(cityName: string) {
    return this.http.get('https://api.openweathermap.org/data/2.5/weather', {
        params: new HttpParams()
        .set('appid', 'db6606399311bc6fb886d73713957114')
        .set('lat', '43.529742')
        .set('lon', '5.447427')
        .set('units', 'metric')
    })
  }


}
