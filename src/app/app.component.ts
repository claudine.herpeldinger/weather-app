import { Component, OnInit } from '@angular/core';
import axios from "axios";
import { WeatherService } from 'src/services/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  
  constructor(private weatherService: WeatherService) {
  }

  weatherData?: any;

  ngOnInit(): void {
    this.weatherData = {
      "coord": {
        "lon": 5.4475,
        "lat": 43.5298
      },
      "weather": [
        {
          "id": 803,
          "main": "Clouds",
          "description": "broken clouds",
          "icon": "04d"
        }
      ],
      "base": "stations",
      "main": {
        "temp": 19.02,
        "feels_like": 18.83,
        "temp_min": 17.71,
        "temp_max": 19.78,
        "pressure": 1022,
        "humidity": 71
      },
      "visibility": 10000,
      "wind": {
        "speed": 2.34,
        "deg": 120,
        "gust": 3.93
      },
      "clouds": {
        "all": 56
      },
      "dt": 1668087711,
      "sys": {
        "type": 2,
        "id": 2006832,
        "country": "FR",
        "sunrise": 1668061513,
        "sunset": 1668097146
      },
      "timezone": 3600,
      "id": 3038354,
      "name": "Aix-en-Provence",
      "cod": 200
    };
    // this.weatherService.getWeather('Aix en Provence').subscribe({
    //   next: (response) => {
    //     this.weatherData = response;
    //     console.log(JSON.stringify(response, null, 2))
    //   }
    // })
  }



}
